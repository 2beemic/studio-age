<?php
$date_r = array("日","月","火","水","木","金","土");



// カレンダーを生成する
function make_cal($y,$m,$d){
	global $date_r;

	$this_m = date("m");
	$ymd = date("Ymd");
	$max_date = get_max_date($y,$m);
	$w = get_start_w($y,$m);
	$calendar_html = <<<EOF
<span id="cal_month">{$y}年{$m}月</span>
<table id="calendar">
	<tr>
EOF;
	// 曜日生成
	for($i = 0;$i <= 6;$i++){
		if($i == 0) $id = "sun";
		elseif($i == 6) $id = "sat";
		else $id = "nor";
		
		$datename = $date_r[$i];
		$calendar_html .= <<<EOF
<td id="{$id}">{$datename}</td>
EOF;
	}
	$calendar_html .= "</tr><tr>";
	
	// 日付追加
	$j = 0;
	

	for($i = 1;$i <= $max_date;$i++){
		$ii = sprintf("%02d",$i);
		/*
		if($j == 1){
			if($start)
				$calendar_html .= <<<EOF
<td id="hol">{$i}<br />休</td>
EOF;
			else
				$calendar_html .= <<<EOF
<td id="hol">休</td>
EOF;
		}
		*/
		if($j == $w || $start){
			$ymii = $y . $m . $ii;
			
			// 盆休など
			if($ymd >= $ymii)
				$input_html = <<<EOF
{$i}
EOF;
/*			elseif(preg_match("/0814$|0815$|0816$/",$ymii))
				$input_html = <<<EOF
{$i}<br /><span style="font-size:8pt;color:#000088;">休</span>
EOF;
*/
			// 満員の場合、その日付を記入する(10月18日)
			elseif(preg_match("/1018$/",$ymii) && date("Ymd") <= "20151018"){
				$input_html = <<<EOF
{$i}<br /><span style="color:#990000;">満</span>
EOF;
			}
			elseif(preg_match("/0101$|0102$|0103$/",$ymii))
				$input_html = <<<EOF
{$i}
EOF;
			else {
				if(!$checked && $m == $this_m) {
					$checked_html = " checked=\"checked\"";
					$checked = true;
				} else
					$checked_html = "";
					
				//営業時間中は翌日の予約は許可
				$hour = date(H);
				$tomorrow = date("Ymd",mktime(0,0,0,date(m),date(d) + 1, date(Y)));
				if($ymii == $tomorrow && ($hour > 18 | $hour <= 10)){
					$input_html = <<<EOF
{$i}
EOF;
				} else {
					$input_html = <<<EOF
<label>{$i}<br /><input type="radio" name="ymd" value="{$y}/{$m}/{$ii}" /></label>
EOF;
				}

				if($m == '11' && $ii == '15') $shitigosan = true;
				elseif($m == '01' && ($ii >= 8 && $ii <= 14) && $j == 1) $seijinshiki = true;
				else {
					$shitigosan = false;
					$seijinshiki = false;
				}

			}
			
			// 七五三なら背景に色を付ける
			if($shitigosan)
				$calendar_html .= <<<EOF
<td id="whi" style="background:#fcc;">{$input_html}<br /><span style="font-size:7pt;">七五三</span></td>
EOF;
			// 成人の日なら背景に色を付ける
			elseif($seijinshiki)
				$calendar_html .= <<<EOF
<td id="whi" style="background:#fcc;">{$input_html}<br /><span style="font-size:7pt;">成人の日</span></td>
EOF;

			else
				$calendar_html .= <<<EOF
<td id="whi">{$input_html}</td>
EOF;
			$start = 1;
		} else {
			$i = 1;
			$calendar_html .= <<<EOF
<td id="whi">&nbsp;</td>
EOF;
		}
		
		$j++;
		if($j >= 7){
		 	$calendar_html .= "</tr><tr>";
			$j = 0;
		}
		if(!$start)
			$i = 0;
		if(!$start && $w == 1)
			$start = 1;
	}
	
	$calendar_html .= "</tr></table>";
	$calendar_html = <<<EOF
<div id="cal">{$calendar_html}</div>
EOF;
	print $calendar_html;
}

// 翌月と翌年を取得
function next_year_month($y,$m){
	$next_month = date("m",mktime(0,0,0,$m+1,1,$y));
	$next_year = date("Y",mktime(0,0,0,$m+1,1,$y));
	
	return array($next_year,$next_month);
}

// 二ヶ月後の月と年を取得
function two_year_month($y,$m){
	$next_month = date("m",mktime(0,0,0,$m+2,1,$y));
	$next_year = date("Y",mktime(0,0,0,$m+2,1,$y));
	
	return array($next_year,$next_month);
}

// 三ヶ月後の月と年を取得
function three_year_month($y,$m){
	$next_month = date("m",mktime(0,0,0,$m+3,1,$y));
	$next_year = date("Y",mktime(0,0,0,$m+3,1,$y));
	
	return array($next_year,$next_month);
}

// 四ヶ月後の月と年を取得
function four_year_month($y,$m){
	$next_month = date("m",mktime(0,0,0,$m+4,1,$y));
	$next_year = date("Y",mktime(0,0,0,$m+4,1,$y));
	
	return array($next_year,$next_month);
}

// 一日の曜日を取得する
function get_start_w($y,$m){
	$w = date("w",mktime(0,0,0,$m,1,$y));
	return $w;
}

// その月の最大の日にちを取得する
function get_max_date($y,$m){
	if($m == "02" || $m == "04" || $m == "06" || $m == "09" || $m == "11"){
		if($m == "02"){
			if($y % 4 == 0 && $y % 100 != 0 || $y % 400)
				$max_date = "28";
			else
				$max_date = "29";
		} else {
			$max_date = "30";
		}
	} else
		$max_date = "31";
	
	return $max_date;
}
?>