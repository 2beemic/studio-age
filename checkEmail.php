<?php
$in = $_GET;

if($in['email']){
	$in['q'] = base64_encode($in['email']);
}

$email = base64_decode($in['q']);

if(!$email){
	header("Content-Type: text/html; charset=utf-8");
	echo "Email などに問題があります";
	exit();
}

$stEmail = "st.age@tryangle.co.jp";
$myEmail = "2beemic@gmail.com";

$subject = "{$email}メールアドレス確認できました";

$date = date("Y/m/d H:i:s");

$message = <<<EOF
{$date}
{$email} の確認が取れました。
EOF;

mb_language("ja");
mb_internal_encoding("UTF-8");

mb_send_mail($stEmail, $subject, $message);
mb_send_mail($myEmail, $subject, $message);


header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>確認が取れました</title>
	<style>
	body {
		width: 100%;
	}
	p {
		width: 90%;
		margin: 3px auto;
	}
	</style>
</head>
<body>
	<h1>ご協力ありがとうございます</h1>
	<p>こちらからメールアドレスにてご連絡させていただきますので、しばらくお待ち下さい。</p>
</body>
</html>
<?php
exit();
// header("Location: https://studio-age.com/thanks.php");
// exit();