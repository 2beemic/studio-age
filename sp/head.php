<?php
if($_SERVER['HTTP_HOST'] == 'localhost'){
	$cdn_server = 'localhost';
} else {
	$cdn_server = 'studio-age.com/sp';
}
?>
    <meta name="description" content="<?php echo_description(); ?>" />
    <meta name="keywords" content="お宮参り,大阪天満宮,記念写真,証明写真,写真スタジオ,フォトスタジオ,写真館" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@age_studio">
	<meta name="twitter:creator" content="@tacayuki_jp">
	<meta name="twitter:title" content="スタジオアージュ">
	<meta name="twitter:description" content="<?php echo_description(); ?>">
	<meta name="twitter:image:src" content="http://studio-age.com/images/ogp.jpg">
 <style>body,html{height:100%}*,:after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}body,html{font-size:100%}body{background:#fff;color:#222;padding:0;margin:0;font-family:"Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;font-weight:400;font-style:normal;line-height:1.5;position:relative}img{max-width:100%;height:auto}img{-ms-interpolation-mode:bicubic}.left{float:left!important}.right{float:right!important}img{display:inline-block;vertical-align:middle}select{width:100%}.row{width:100%;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0;max-width:62.5rem}.row:after,.row:before{content:" ";display:table}.row:after{clear:both}.columns{padding-left:.9375rem;padding-right:.9375rem;width:100%;float:left}@media only screen{.columns{position:relative;padding-left:.9375rem;padding-right:.9375rem;float:left}.small-12{width:100%}}@media only screen and (min-width:40.063em){.columns{position:relative;padding-left:.9375rem;padding-right:.9375rem;float:left}}@media only screen and (min-width:64.063em){.columns{position:relative;padding-left:.9375rem;padding-right:.9375rem;float:left}}label{font-size:.875rem;color:#4d4d4d;display:block;font-weight:400;line-height:1.5;margin-bottom:0}input[type=text]{-webkit-appearance:none;border-radius:0;background-color:#fff;font-family:inherit;border-style:solid;border-width:1px;border-color:#ccc;box-shadow:inset 0 1px 2px rgba(0,0,0,.1);color:rgba(0,0,0,.75);display:block;font-size:.875rem;margin:0 0 1rem 0;padding:.5rem;height:2.3125rem;width:100%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}select{-webkit-appearance:none!important;border-radius:0;background-color:#fafafa;background-image:url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeD0iMTJweCIgeT0iMHB4IiB3aWR0aD0iMjRweCIgaGVpZ2h0PSIzcHgiIHZpZXdCb3g9IjAgMCA2IDMiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDYgMyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PHBvbHlnb24gcG9pbnRzPSI1Ljk5MiwwIDIuOTkyLDMgLTAuMDA4LDAgIi8+PC9zdmc+);background-position:100% center;background-repeat:no-repeat;border-style:solid;border-width:1px;border-color:#ccc;padding:.5rem;font-size:.875rem;font-family:"Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;color:rgba(0,0,0,.75);line-height:normal;border-radius:0;height:2.3125rem}select::-ms-expand{display:none}input[type=checkbox],input[type=radio],select{margin:0 0 1rem 0}.fixed{width:100%;left:0;position:fixed;top:0;z-index:99}.top-bar{overflow:hidden;height:2.8125rem;line-height:2.8125rem;position:relative;background:#333;margin-bottom:0}@media only screen and (min-width:40.063em){.top-bar{background:#333;overflow:visible}.top-bar:after,.top-bar:before{content:" ";display:table}.top-bar:after{clear:both}}div,h2,h3,li,p,ul{margin:0;padding:0}a{color:#008cba;text-decoration:none;line-height:inherit}a img{border:none}p{font-family:inherit;font-weight:400;font-size:1rem;line-height:1.6;margin-bottom:1.25rem;text-rendering:optimizeLegibility}h2,h3{font-family:"Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;font-weight:400;font-style:normal;color:#222;text-rendering:optimizeLegibility;margin-top:.2rem;margin-bottom:.5rem;line-height:1.4}h2{font-size:1.6875rem}h3{font-size:1.375rem}i{font-style:italic;line-height:inherit}strong{font-weight:700;line-height:inherit}ul{font-size:1rem;line-height:1.6;margin-bottom:1.25rem;list-style-position:outside;font-family:inherit}ul{margin-left:1.1rem}@media only screen and (min-width:40.063em){h2,h3{line-height:1.4}h2{font-size:2.3125rem}h3{font-size:1.6875rem}}.off-canvas-wrap{-webkit-backface-visibility:hidden;position:relative;width:100%;overflow:hidden}.inner-wrap{position:relative;width:100%}.inner-wrap:after,.inner-wrap:before{content:" ";display:table}.inner-wrap:after{clear:both}.right-off-canvas-menu{-webkit-backface-visibility:hidden;width:15.625rem;top:0;bottom:0;position:absolute;overflow-x:hidden;overflow-y:auto;background:#333;z-index:1001;box-sizing:content-box;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar;-ms-transform:translate(100%,0);-webkit-transform:translate3d(100%,0,0);-moz-transform:translate3d(100%,0,0);-ms-transform:translate3d(100%,0,0);-o-transform:translate3d(100%,0,0);transform:translate3d(100%,0,0);right:0}.right-off-canvas-menu *{-webkit-backface-visibility:hidden}.textfieldInvalidFormatMsg,.textfieldRequiredMsg{display:none}body{background-color:#fff!important}.top-bar{background-color:#654136;color:#fff;border-bottom:3px solid #9abec9}.top-bar img{width:36px;height:36px}.logo{margin-left:15px}.logo img{width:90px;height:auto}.orange{color:#fc9632}.right-off-canvas-menu{position:fixed;right:-250px;z-index:10000;-moz-transform:none;-ms-transform:none;-webkit-transform:none;transform:none}a{line-height:2rem;color:#00a7e0;font-size:18px}h3.no-bg{margin-left:-5px;background:#fff}h3.no-bg:first-child{margin-left:0}.top_images{padding-top:50px;width:100%;height:200px;background-color:#b07b6b;background-repeat:no-repeat;background-image:url(../images/index_top.jpg);background-size:contain;background-position:left 40px}.top_images p{font-weight:700;text-shadow:1px 1px 1px #191919;font-size:12px;text-align:right;margin-top:0;margin-left:32vw;margin-right:15px;color:#fff}.top_images p a{width:60vw;color:#fff}.well{width:100vw;background-color:#e9e9e9;color:#000;padding-left:15px;padding-right:15px;margin-bottom:0;font-style:12px}h2{padding:10px 0 10px 20px;font-size:18px;color:#fff;background-color:#fff;text-shadow:0 2px 0 #231612;background-repeat:no-repeat;background-image:url(../images/h2-background.png);background-size:contain;background-position:top left;background:#fff url(../images/h2-background.png) top left/100vw auto no-repeat}h3{font-size:1.15rem}form label{font-weight:700;background-color:#e9e9e9}.ask_jump{border-radius:50%;box-shadow:0 4px 0 #934a02;padding-top:6px;width:75px;height:75px;display:block;position:fixed;right:16px;bottom:16px;background-color:#fc9632;text-shadow:none;text-align:center}.ask_jump a{color:#fff!important;line-height:1rem;text-shadow:none}input,select{font-size:18px!important}input[type=submit]{border-radius:5px;width:80%;margin:0 auto 1.5em!important}
     .center{display:flex;width:900px;margin:0 auto}.center ul{list-style: none}.center ul li{display:inline-block;padding:5px 1em}.center ul li a{color: white}.center+.right{margin-top: -2.5em}@media screen and (max-width: 1010px){.center{display:none;}.center + .right{margin-top:0;}}</style>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "ProfessionalService",
        "name": "スタジオアージュ",
        "image": "https://studio-age.com/images/ogp.jpg",
        "@id": "@age_studio",
        "url": "https://studio-age.com",
        "telephone": "06-6881-0170",
        "priceRange": "¥800-¥41500",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "大阪市北区天神橋2丁目3番22号西川ビル2F",
            "addressLocality": "大阪府",
            "postalCode": "530-0041",
            "addressCountry": "JP"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": 34.6969429,
            "longitude": 135.51161230000002
        },
        "openingHoursSpecification": {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
            ],
            "opens": "10:00",
            "closes": "18:00"
        }
    }
</script>
	<link rel="shortcut icon" href="//studio-age.com/favicon.ico">
	<link rel="apple-touch-icon" href="/images/apple-touch-icon.png">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139892740-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-139892740-1');
</script>
<script async
        src="https://s.yimg.jp/images/listing/tool/cv/ytag.js"></script>
<script>
    window.yjDataLayer = window.yjDataLayer || [];
    function ytag() { yjDataLayer.push(arguments); }
    ytag({"type":"ycl_cookie"});
</script>