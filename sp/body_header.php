  <div>
    <div class="inner-wrap">
      <div class="fixed">
        <nav class="top-bar" data-topbar role="navigation">
          <div class="left">
            <div class="logo">
              <a href="/sp/"><img src="//<?php echo $cdn_server; ?>/images/logo.svg" alt="スタジオ・アージュ"></a>
            </div>
          </div>
            <div class="center">
                <ul>
                    <li><a href="shrines.html">お宮参り写真</a></li>
                    <li><a href="753.html">七五三写真</a></li>
                    <li><a href="costume.html">無料レンタル衣装</a></li>
                    <li><a href="portrait.html">その他記念写真</a></li>
                    <li><a href="portrait-deceased.html">肖像・遺影写真</a></li>
                    <li><a href="id_photo.html">証明写真</a></li>
                </ul>
            </div>
          <div class="right">
            <a href="#footer_menu"><img src="//<?php echo $cdn_server; ?>/images/menu_icon.svg" alt="メニュー"></a>
          </div>
        </nav>
      </div>
<!--      Off Canvas Menu -->
<!--      <nav class="right-off-canvas-menu">-->
<!--         whatever you want goes here -->
<!--          <ul>-->
<!--            --><?php //include("menu.php"); ?>
<!--          </ul>-->
<!--      </nav>-->