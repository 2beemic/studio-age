    <div class="ask_jump" id="ask_jump" class="fadeOutRight animated"><a href="tel:0668810170">電話<br>問合せ<br><i class="fa fa-phone"></i></a></div>
    <!-- close the off-canvas menu -->
  </div>
<footer>
  <a class="scroll-top" id="js-button"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>

  <p>&copy; <?php echo date("Y"); ?> スタジオ・アージュ</p>
</footer>
    <link href="/sp/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/dist/style.css">
    <link rel="stylesheet" href="/sp/css/app.css">
    <script src="/js/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="/sp/js/lazyestload.js"></script>
    <script src="/dist/main.js" defer></script>
<!--  <script src="/js/SpryValidationTextField.js" type="text/javascript" defer></script>-->
  <script src="/js/jquery.easing.min.js" defer></script>
  <script src="./js/studio-age.js" defer></script>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-60502826-1', 'auto');
    ga('send', 'pageview');
  </script>
</body>
</html>