<?php
// header("Content-Type: text/html; charset=UTF-8");
// 送信先メールアドレス
// $mailto = "reserves@tryangle.co.jp"; // pass: 72T662
//$mailto = "2beemic@gmail.com";
//
$ua = getenv('HTTP_USER_AGENT');
if(preg_match("/iphone|android|mobile/i",$ua)){
	$sp = "true";
}
$fromto = "st.age@tryangle.co.jp";
$mailto = $fromto; // とりあえずメインメール宛

// keyを日本語化
if(empty($_POST['ccp'])) $key_r = array("name"=>"名前","telephone"=>"電話番号","email" => "メールアドレス","comment"=>"お問い合わせ内容");
else $key_r = array("name"=>"会社名(名前)","telephone"=>"電話番号","email" => "メールアドレス","comment"=>"お問い合わせ内容");
// 安全化
if($_POST) $in = $_POST;
else $in = $_GET;

if(isset($in['services'])){
	$services_r = safty($in['services']);
}

if($in['type'] != 'お問い合わせ') {
	$type = 'online_booking';
	if(!$services_r && !$in['kind_photo']){
		error('ご希望のサービスが選択されていません');
	}
} else {
	$type = 'ask';
}

$in['email'] = mb_convert_kana($in['email'],"KVa","UTF-8");
if($in['email'] == null) {
	header("Location: http://studio-age.com/");
	exit;
}

// テスト送信
if($in['email'] == '2beemic@gmail.com') {
	$mailto = $fromto = '2beemic@gmail.com';
	$test = "true";
}

if($in['kind_photo'] && $in['num_pose']){
	$services_r[] .= <<<EOF
記念写真 : {$in['num_pose']}ポーズ{$in['num_set']}セット　大人 {$in['num_otona']}名様 子供 {$in['num_kodomo']}名様
撮影内容 : {$in['kind_photo']}{$in['kind_photo_etc']}
EOF;
} else {
	$services_r[] .= <<<EOF
撮影内容 : {$in['kind_photo']}{$in['kind_photo_etc']}
EOF;
}

/*
// 確認
if(!$in['check']){
	include("./reserves_check.php");
	exit;
}
*/
// ◆ 確認済み
// メールをこちらに送る
$services_mail_body = "[サービス内容]\n";
foreach($services_r as $services){
	$services_mail_body .= <<<EOF
{$services}\n
EOF;
	$customer_mail_body .= <<<EOF
{$services}\n
EOF;
}
foreach($in as $key => $val){
	$jp_key = $key_r[$key];
	if(!$jp_key) continue;
	$mail_body .= <<<EOF
{$jp_key} : {$val}\n
EOF;
}
// 日時を整形
if(!isset($in['time'])) $in['time'] = $in['hour'] . ":" . $in['minute'];
if($in['year']) $date = $in['year'] . "年" . $in['month'] . "月" . $in['date'] . "日 " . $in['hour'] . "時" . $in['minute'] . "分";
elseif($in['ymd']) $date = $in['ymd'] . " " . $in['time'];
else $date = $in['ymd'] . " " . $in['hour'] . ":" . $in['minute'];


$remote_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);

if(empty($in['ccp'])){
	$mail_body = <<<EOF
{$mail_body}
性別 : {$in['sex']}
日時 : {$date}
{$services_mail_body}
EOF;
} else {
	$mail_body = <<<EOF
{$mail_body}
{$services_mail_body}
EOF;
}

$mail_body .= <<<EOF
//
リモートホスト : {$remote_host}
IPアドレス : {$_SERVER['REMOTE_ADDR']}
ブラウザ : {$_SERVER['HTTP_USER_AGENT']}
EOF;


mb_language("ja");
mb_internal_encoding("UTF-8");
// 件名
if($type == 'ask')
	$subject = "【問合せ】 {$in['name']}様";
elseif(empty($in['ccp']))
	$subject = "【予約】{$in['name']}様 {$date}";
else
	$subject = "チラシ・カタログ・ロゴデザイン({$in['name']}様)";
// メール送信

if(isset($test)){
	mb_send_mail("himenaotaro@i.softbank.jp",$subject,"TEST MODE is On.");;
} else {
	mb_send_mail("st.age@tryangle.co.jp",$subject,$mail_body);
	mb_send_mail("webmaster@tryangle.co.jp",$subject,$mail_body);
	mb_send_mail("reserves@tryangle.co.jp",$subject,$mail_body);

	mb_send_mail("tryangle@k8.dion.ne.jp",$subject,$mail_body);
}

mb_send_mail("2beemic@gmail.com",$subject,$mail_body);

// お客さまにメール送信
if(!preg_match("/docomo\.ne\.jp$|ezweb\.ne\.jp|softbank\.ne\.jp|jp-(\w)\.ne\.jp|vodafone\.ne\.jp|emnet|pdx\.ne\.jp/i",$in['email']))
	$pc = true;
else
	$mobile = true;
if($in['ccp']){
	$mail_body = <<<EOF
{$in['name']}様

スタジオ アージュ　サポート担当です。
このたびは当スタジオへお問い合わせいただき、ありがとうございます。

後ほど確認メールをお送りさせていただきます。
なお、営業時間外にお問い合わせいただいた場合は
翌日返答とさせていただきます。

ご質問のお客様へは、ご質問に関して後ほど返信させていただきます。
ご質問内容によっては数日かかる場合がございます、あらかじめご了承下さい。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

スタジオ アージュ

住所：大阪市北区天神橋2丁目3-22 西川ビル2F http://studio-age.com/maps.html
 URL：http://studio-age.com/
 TEL：06-6881-0170
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


下記内容に関して、確かに承りました。

………………………………………………………………………………………………
この度はスタジオアージュへお問い合わせいただき、誠にありがとうございます。
本メールはお客様にご確認して頂くために発信される、自動配信メールです。
………………………………………………………………………………………………
{$customer_mail_body}
EOF;
} else {
	$mail_body = <<<EOF
{$in['name']}様

スタジオ アージュ　サポート担当です。
このたびは当スタジオへお問い合わせいただき、ありがとうございます。

ご予約のお客様は、後ほどご予約日時等の確認メールをお送りさせていただきます。
なお、営業時間外にお問い合わせいただいた場合は
翌日返答とさせていただきます。

ご質問のお客様へは、ご質問に関して後ほど返信させていただきます。
ご質問内容によっては数日かかる場合がございます、あらかじめご了承下さい。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

スタジオ アージュ　担当 平家 ひろゆき

住所：大阪市北区天神橋2丁目3-22 西川ビル2F http://studio-age.com/maps.html
 URL：http://studio-age.com/
 TEL：06-6881-0170
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


下記内容に関して、確かに承りました。

………………………………………………………………………………………………
この度はスタジオアージュへお問い合わせいただき、誠にありがとうございます。
本メールはお客様にご確認して頂くために発信される、自動配信メールです。
………………………………………………………………………………………………
{$customer_mail_body}
ご予約日時 : {$date}
EOF;
}

if($in['type'] == 'お問い合わせ')
	$subject = "お問い合わせありがとうございます[スタジオアージュ]";
elseif(empty($in['ccp'])) $subject = "ご予約ありがとうございます[スタジオアージュ]";
else $subject ="お問い合わせありがとうございます[スタジオアージュ]";

// 送信元
$headers = <<<EOF
From: {$fromto}\n
EOF;

mb_send_mail($in['email'],$subject,$mail_body,$headers);

if(isset($test) && isset($sp))
	header("Location: https://studio-age.com/sp/maps.html?v=form&q=SORRY__this_is_TEST_MODE");
elseif(isset($sp))
	header("Location: https://studio-age.com/sp/maps.html?v=form");
elseif(isset($test))
	header("Location: https://studio-age.com/thanks.html?SORRY__this_is_TEST_MODE");
elseif(empty($in['ccp']))
	header("Location: https://studio-age.com/thanks.html");
else
	header("Location: https://studio-age.com/ccp_thanks.html");
exit;


function error($str){
	print <<<EOF
{$str}<br />
<a href="javascript:history.back();">go back</a>
EOF;
	exit;
}

function safe($in){
	$in = @htmlspecialchars($in);
	return $in;
}

function safty($in){
	foreach($in as $key => $val){
		$val = safe($val);
		$val = mb_convert_kana($val, "KVa", "utf-8");
		$in[$key] = $val;
	}
	return $in;
}
?>