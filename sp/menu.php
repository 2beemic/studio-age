<!-- whatever you want goes here -->
<ul>
    <li><label><a href="/sp/" rel="noopener">St.<span class="orange">â</span>ge</a></label></li>
    <li><a href="maps.html" rel="noopener">アクセス(地図・場所)</a></li>
    <li><a href="price.html" rel="noopener">料金一覧</a></li>
    <li><a href="portrait.html" rel="noopener">記念写真(お宮参り七五三前撮りなど)</a></li>
    <li>
        <ul>
            <li><a href="costume.html" rel="noopener">無料レンタル衣装</a></li>
            <li><a href="costume-omiya.html" rel="noopener">無料レンタル産着</a></li>
            <li><a href="costume-shichigosan.html" rel="noopener">無料レンタル七五三着</a></li>
            <li><a href="portrait-deceased.html" rel="noopener">終活・遺影写真</a></li>
            <li><a href="word-of-mouth_portrait.html" rel="noopener">お客様の声・口コミ</a></li>
        </ul>
    </li>
    <li><a href="id_photo.html" rel="noopener">証明写真 プロフィール写真</a></li>
    <li>
        <ul>
            <li><a href="my-number-card.html" rel="noopener">マイナンバー(個人番号)カード</a></li>
            <li><a href="word-of-mouth_id-photo.html" rel="noopener">お客様の声・口コミ</a></li>
        </ul>
    </li>
    <li><a href="shrines.html" rel="noopener">大阪天満宮・お宮参り写真</a></li>
    <li><a href="753.html" rel="noopener">大阪天満宮・七五三写真</a></li>
    <li><a href="http://ameblo.jp/studioage/" target="_blank" rel="noopener">St.age ブログ <i
                    class="fa fa-noopener-link"></i></a></li>
    <li><a href="#form">お問い合わせ</a></li>
    <li><a href="https://line.me/ti/p/7_fJk59rmN">お問い合わせ（LINE）</a></li>
    <li><a href="faq.html">よくあるご質問</a></li>
    <li><a href="about.html" rel="noopener">会社概要</a></li>
    <li><a href="privacy.html" rel="noopener">プライバシーポリシー</a></li>
    <li><a href="./images/price-profile.png">撮影料金リスト</a></li>
    <li><a href="./images/price-yakimashi.png">焼き増しリスト</a></li>
    <li><a href="./images/price-kitsuke.png">着付けリスト</a></li>
    <li>
        <ul class="sns_buttons">
            <!--                <li class="icon"><a href="twitter://post?message=<?php the_title(); ?>%20<?php the_permalink(); ?>%20" rel="noopener" target="_blank"><img src="//<?php echo $cdn_server; ?>/images/icon_twitter.svg" alt="Twitter でつぶやく"></a></li> -->
            <li class="icon"><a
                        href="https://twitter.com/intent/tweet?original_referer=<?php the_permalink(); ?>&amp;ref_src=twsrc%5Efw&amp;text=<?php the_title(); ?>%20<?php the_permalink(); ?>%20&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?>"
                        rel="noopener" target="_blank"><img
                            src="//<?php echo $cdn_server; ?>/images/icon_twitter.svg" alt="Twitter でつぶやく"></a></li>
            <li class="icon"><a class="nofb-like"
                                href="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>"
                                target="_blank" rel="noopener"><img
                            src="//<?php echo $cdn_server; ?>/images/icon_facebook.svg" alt="いいね!"></a></li>
            <li class="icon"><a href="https://line.me/ti/p/7_fJk59rmN"
                                rel="noopener"><img src="//<?php echo $cdn_server; ?>/images/icon_line.svg"
                                                               alt="LINE で知らせる"></a></li>
        </ul>
    </li>
</ul>