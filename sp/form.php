<form method="post" action="/reserves.php" id="form" data-ajax=”false”>
    <h2>
        ご予約・お問い合わせ
    </h2>
    <div class="row" data-role="fieldcontain" data-theme="a">
        <div class="small-12 columns">
            <?php
            $hour = date("H");
            if ($hour < 10) {
                $aft = 10 - $hour;
                $alert = "、本日の電話の受付まであと約<strong>{$aft}時間後</strong>です)";
            } elseif ($hour > 18) {
                $alert = "、<strong>本日の電話の受付は終了しました</strong>)";
            } else {
                $now = date("H時i分");
                $alert = "、<strong>{$now} 現在、受付中です</strong>)";
            }
            ?>
            <p>
                電話・FAX・E-mailでのご予約・お問い合わせにも対応しております。お気軽にご連絡下さい。(電話は朝10時〜夜18時まで<?php echo $alert; ?>
            </p>
            <ul>
                <li><a href="https://line.me/ti/p/7_fJk59rmN">LINE</a></li>
                <li><a href="tel:0668810170">TEL/FAX 06-6881-0170</a></li>
                <li><a href="mailto:st.age@tryangle.co.jp"><span>st.age@tryangle.co.jp</span> </a></li>
            </ul>
            <h3>フォームでのお問い合わせ</h3>
            <p>
                ※SSLで保護されています。<br>
                ご登録されたメールアドレスに、予約確認メールを送らせていただいております。 メールフィルターをされてる場合は「st.age@tryangle.co.jp」を受信許可設定にしてください。<br>
                24時間以内に返信がない場合は、メールが届いていない可能性があります。お手数ですが再度ご連絡ください。</span>
            </p>

            <!-- p>
              facebook ログインで名前とメールアドレスを自動入力する→<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>
            </p -->
            <input type="hidden" type="facebook" id="facebookid" value="none">

            <legend>お申込み内容</legend>
            <fieldset data-role="controlgroup" data-type="horizontal">
                <label for="reserved">
                    <input type="radio" id="reserved" value="ご予約" name="type" checked="checked"> ご予約</label>
                <label for="inquire">
                    <input type="radio" id="inquire" value="お問い合わせ" name="type"> お問合わせ
                </label>
                <label for="request">
                    <input type="radio" id="request" value="ご要望" name="type"> ご要望
                </label>
            </fieldset>
            <fieldset>
                <legend>サービス内容</legend>
                <label for="portrait"><input type="radio" name="services" id="portrait" value="記念写真・お宮参り" checked="checked"> 記念写真・お宮参り</label><br>
                <label for="portrait"><input type="radio" name="services" id="portrait" value="記念写真・七五三" checked="checked"> 記念写真・七五三</label><br>
                <label for="portrait"><input type="radio" name="services" id="portrait" value="記念写真・その他" checked="checked"> 記念写真・その他</label><br>
                <label for="id_photo"><input type="radio" name="services" id="id_photo" value="証明写真"> 証明写真（就職・就活写真・資格写真など）</label><br>
                <label for="des"><input type="radio" name="services" id="des" value="遺影・肖像写真"> 遺影・肖像写真</label><br>
                <label for="etc"><input type="radio" name="services" id="etc" value="その他"> その他</label>
            </fieldset>


                <legend for="name"><label for="name">お名前 <span class="orange">(必須)</span></label></legend>
            <span id="sprytextfield222">
              <input type="text" name="name" id="name" size="25" placeholder="お名前" required>
              <span class="textfieldRequiredMsg">お名前を指定する必要があります。</span>
            </span>

            <fieldset data-role="controlgroup">
                <legend><label for="sex">性別 <span class="orange">(必須)</span></label></legend>
                <select name="sex" id="sex">
                    <option label="男性" value="男性">男性</option>
                    <option label="女性" value="女性" selected="selected">女性</option>
                </select>
            </fieldset>

            <span id="sprytextfield1">
              <legend for="telephone"><label for="telephone">電話番号  <span class="orange">(必須)</span></label></legend>
              <input type="tel" name="telephone" id="telephone" size="25" placeholder="電話番号(例.09012341234)" required>
                <span class="textfieldRequiredMsg">電話番号を指定する必要があります。</span>
                <span class="textfieldInvalidFormatMsg">無効な形式です。</span>
            </span>

            <span id="sprytextfield2">
            <legend for="email"><label for="email">メールアドレス <span class="orange">(必須)</span></label></legend>
              <label>
                <input type="email" name="email" id="email" size="50" placeholder="メールアドレス" required>
              </label>
            </span>
            <span class="textfieldRequiredMsg">メールアドレスを入力してください。</span>
            <span class="textfieldInvalidFormatMsg">メールアドレスが不正です。</span>
            <span id="sprytextfield3">
            <legend for="email"><label for="email2">メールアドレス(確認用) <span class="orange">(必須)</span></label></legend>
              <label>
                <input type="email" name="email2" id="email2" size="50" placeholder="メールアドレス" required>
              </label>
            </span>
            <div id="reserved_part">
                <?php
                $this_year = date("Y");
                $two_days_later = date("Y-m-d", time() + (2 * 24 * 60 * 60));
                $tomorrow_later = date("Y-m-d", time() + (1 * 24 * 60 * 60));
                $today = date("Y-m-d");
                ?>
                <legend>撮影希望日</legend>
                <label for="year">年</label>
                <select name="year" id="year">
                    <option value="--年">年を選択してください</option>
                    <?php foreach(range(date("Y"),date("Y",mktime(0,0,0,12,31,date("Y")+2))) as $year): ?>
                    <option value="<?php echo $year; ?>" label="<?php echo $year; ?>"><?php echo $year; ?>年</option>
                    <?php endforeach; ?>
                </select>
                <label for="month">月</label>
                <select name="month" id="month">
                    <option value="--月">月を選択してください</option>
                    <?php foreach(range(1,12) as $month): ?>
                        <option value="<?php echo $month; ?>" label="<?php echo $month; ?>"><?php echo $month; ?>月</option>
                    <?php endforeach; ?>
                </select>
                <label for="date">日</label>
                <select name="date" id="date">
                    <option value="--日にち">日にちを選択してください</option>
                    <?php foreach(range(1,31) as $date): ?>
                        <option value="<?php echo $date; ?>" label="<?php echo $date; ?>"><?php echo $date; ?>日</option>
                    <?php endforeach; ?>
                </select>
<!--                <input type="text" name="ymd" data-role="datebox" placeholder="ご指定の日時をご入力ください"-->
<!--                       data-options='{"mode": "calbox","notToday": "true", "afterToday":"true" , "blackDates": ["--><?php //echo $two_days_later . '","' . $tomorrow_later; ?><!--", "2015-10-18"], "useFocus": true}'>-->

                <legend>時間</legend>
                <label for="hour">時</label>
                <select name="hour" id="hour">
                    <?php foreach(range(10,17) as $hour): ?>
                    <option value="<?php echo $hour; ?>" label="<?php echo $hour; ?>"><?php echo $hour; ?>時</option>
                    <?php endforeach; ?>
                </select>
                <label for="minute">分</label>
                <select name="minute" id="minute">
                    <?php foreach(range(0,55,5) as $minute): ?>
                    <option value="<?php $minute = str_pad($minute,2,0,STR_PAD_LEFT); echo $minute; ?>" label="<?php echo $minute; ?>"><?php echo $minute; ?>分</option>
                    <?php endforeach; ?>
                </select>
<!--                <input name="time" type="text" data-role="datebox" placeholder="ご指定の時間をご入力ください"-->
<!--                       data-options='{"mode":"timeflipbox", "minHour":"10", "maxHour":"17", "minuteStep":"5", "overrideTimeOutput":"%H:%M(%p)", "useFocus": true}'>-->
                <p>営業時間 10:00～18:00<br>※ 同時刻で予約が重なった場合は、先着順とさせて頂きます。その場合はこちらからご連絡させて頂きます。</p>

<!--                    <label><a href="#button__portrait"><input type="button" id="button__portrait" value="【記念写真】"-->
<!--                                                              style="border:1px solid silver;width:100%;margin:0 auto"></a></label>-->
<!--                    <div id="show_portrait" style="display: none;">-->
<!--                        <select name="num_pose">-->
<!--                            <option value="" selected="selected">ポーズ数</option>-->
<!--                            <option value="1">1ポーズ</option>-->
<!--                            <option value="2">2ポーズ</option>-->
<!--                            <option value="3">3ポーズ</option>-->
<!--                            <option value="4">4ポーズ</option>-->
<!--                        </select>-->
<!--                        <select name="num_set">-->
<!--                            <option value="" selected="selected">冊数(セット数)</option>-->
<!--                            <option value="1">1冊(セット)</option>-->
<!--                            <option value="2">2冊(セット)</option>-->
<!--                            <option value="3">3冊(セット)</option>-->
<!--                            <option value="4">4冊(セット)</option>-->
<!--                        </select><br>-->
<!--                        撮影人数<br>-->
<!--                        <select name="num_otona">-->
<!--                            <option value="" selected="selected">大人の人数</option>-->
<!--                            <option value="0">大人 0人</option>-->
<!--                            <option value="1">大人 1人</option>-->
<!--                            <option value="2">大人 2人</option>-->
<!--                            <option value="3">大人 3人</option>-->
<!--                            <option value="4">大人 4人</option>-->
<!--                            <option value="5">大人 5人</option>-->
<!--                            <option value="6">大人 6人</option>-->
<!--                            <option value="7">大人 7人</option>-->
<!--                            <option value="8">大人 8人</option>-->
<!--                            <option value="9">大人 9人</option>-->
<!--                        </select>-->
<!--                        <select name="num_kodomo">-->
<!--                            <option value="" selected="selected">子供の人数</option>-->
<!--                            <option value="0">子供 0人</option>-->
<!--                            <option value="1">子供 1人</option>-->
<!--                            <option value="2">子供 2人</option>-->
<!--                            <option value="3">子供 3人</option>-->
<!--                            <option value="4">子供 4人</option>-->
<!--                            <option value="5">子供 5人</option>-->
<!--                            <option value="6">子供 6人</option>-->
<!--                            <option value="7">子供 7人</option>-->
<!--                            <option value="8">子供 8人</option>-->
<!--                            <option value="9">子供 9人</option>-->
<!--                        </select><br><br>-->
<!--                        撮影内容<br>-->
<!--                        <label><input type="radio" name="kind_photo" value="お宮参り・百日祝い">お宮参り・百日祝い</label>-->
<!--                        <label><input type="radio" name="kind_photo" value="七五三">七五三</label>-->
<!--                        <label><input type="radio" name="kind_photo" value="終活・遺影写真">終活・遺影写真</label>-->
<!--                        <label><input type="radio" name="kind_photo" value="その他">その他</label>-->
<!--                        <input type="text" name="kind_photo_etc" placeholder="「その他」の方は内容をご記入下さい"><br><br>-->
<!---->
<!--                        記念写真オプション<br>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox1" value="着付け訪問着(記念写真オプション)">-->
<!--                            着付け訪問着(<span style="font-weight:bold;">記念写真</span>オプション)</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox2" value="着付け七五三(記念写真オプション)">-->
<!--                            着付け七五三(<span style="font-weight:bold;">記念写真</span>オプション)</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox3" value="ヘアーセット・大人(記念写真オプション)">-->
<!--                            ヘアーセット・大人(<span style="font-weight:bold;">記念写真</span>オプション)</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox4" value="ヘアーセット・小人(記念写真オプション)">-->
<!--                            ヘアーセット・小人(<span style="font-weight:bold;">記念写真</span>オプション)</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox5" value="ヘアーセット＆メイク・大人(記念写真オプション)">-->
<!--                            ヘアーセット＆メイク・大人(<span style="font-weight:bold;">記念写真</span>オプション)</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox6" value="ヘアーセット＆メイク・小人(記念写真オプション)">-->
<!--                            ヘアーセット＆メイク・小人(<span style="font-weight:bold;">記念写真</span>オプション)</label>-->
<!--                        <span id="com">※着付け、ヘア＆メイクは当スタジオの隣の部屋で行います。</span>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox7" value="訪問着レンタル ￥5,000〜">-->
<!--                            訪問着レンタル ￥5,000〜</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox8" value="産着レンタル">-->
<!--                            産着レンタル(<span style="color:red">サービス内容</span>)</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox9" value="男の子">-->
<!--                            男の子</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox10" value="女の子">-->
<!--                            女の子</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox11" value="七五三お子様着物レンタル">-->
<!--                            七五三お子様着物レンタル(<span style="color:red">サービス内容</span>)</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox12" value="男の子">-->
<!--                            男の子</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox13" value="女の子">-->
<!--                            女の子</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox14" value="3歳">-->
<!--                            3歳</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox15" value="5歳">-->
<!--                            5歳</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox16" value="7歳">-->
<!--                            7歳</label>-->
<!--                    </div>-->
<!---->
<!--                    <label><a href="#button__shoumei"><input type="button" id="button__shoumei" value="【証明写真】"-->
<!--                                                             style="border:1px solid silver;width:100%;margin:0 auto"></a></label>-->
<!--                    <div id="show_shoumei" style="display:none;">-->
<!--                        <label><input type="radio" name="services[]" value="おすすめ！就職活動サポートコース(30)">おすすめ！就職活動サポートコース(30)(平日割で3,500円)</label>-->
<!--                        <label><input type="radio" name="services[]"-->
<!--                                      value="おすすめ！就職活動サポートコース(20)">就職活動サポートコース(20)</label>-->
<!--                        <label><input type="radio" name="services[]"-->
<!--                                      value="おすすめ！就職活動サポートコース(10)">就職活動サポートコース(10)</label>-->
<!--                        <label><input type="radio" name="services[]" value="おすすめ！就職活動サポートコース(4)">就職活動サポートコース(4)</label>-->
<!--                        <label><input type="radio" name="services[]"-->
<!--                                      value="マイナンバー・個人番号カード証明写真">マイナンバー・個人番号カード証明写真</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox17" value="ヘア＆フルメイク">-->
<!--                            ヘア＆フルメイク</label> <label><input type="checkbox" name="services[]" id="checkbox01"-->
<!--                                                           value="ヘア＆ポイントメイク">-->
<!--                            ヘア＆ポイントメイク</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox18" value="ビザ用">ビザ用</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox19" value="パスポート">パスポート</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox20"-->
<!--                                      value="エコノミーコース">エコノミーコース</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox21" value="裁断あり(+1,000円)">裁断あり(+1,000円)</label>-->
<!--                        ※ ヘア＆メイクは当スタジオ提携の美容院にてお直し致します。-->
<!--                        【焼き増し】<br>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox22" value="焼き増し 証明写真 4枚">-->
<!--                            焼き増し 証明写真 4枚</label> <label><input type="checkbox" name="services[]" id="checkbox02"-->
<!--                                                               value="焼き増し 証明写真 10枚">-->
<!--                            焼き増し 証明写真 10枚</label> <label><input type="checkbox" name="services[]" id="checkbox03"-->
<!--                                                                value="焼き増し 証明写真 20枚">-->
<!--                            焼き増し 証明写真 20枚</label> <label><input type="checkbox" name="services[]" id="checkbox04"-->
<!--                                                                value="焼き増し 証明写真 30枚">-->
<!--                            焼き増し 証明写真 30枚</label> <label><input type="checkbox" name="services[]" id="checkbox05"-->
<!--                                                                value="焼き増し L版サイズ 1枚">-->
<!--                            焼き増し L版サイズ 1枚</label> <label><input type="checkbox" name="services[]" id="checkbox06"-->
<!--                                                                value="焼き増し 2L版サイズ 1枚">-->
<!--                            焼き増し 2L版サイズ 1枚</label> <label><input type="checkbox" name="services[]" id="checkbox70"-->
<!--                                                                 value="焼き増し 六つ切りサイズ 1枚">-->
<!--                            焼き増し 六つ切りサイズ 1枚</label> <label><input type="checkbox" name="services[]" id="checkbox08"-->
<!--                                                                  value="焼き増し データCD 1枚">-->
<!--                            焼き増し データCD 1枚</label> <label><input type="checkbox" name="services[]" id="checkbox09"-->
<!--                                                                value="焼き増し データSD 1枚">-->
<!--                            焼き増し データSD 1枚</label>-->
<!--                    </div> -->
<!--                    <label><a href="#button__etc"><input type="button" id="button__etc" value="【その他】"-->
<!--                                                         style="border:1px solid silver;width:100%;margin:0 auto"></a></label>-->

<!--                    <div id="show_etc" style="display: none;">-->
<!--                        <label><input type="checkbox" name="services[]" value="終活・遺影写真">終活・遺影写真</label>-->
<!--                        <label><input type="checkbox" name="services[]" id="checkbox23" value="商品撮影">-->
<!--                            商品撮影</label> <label><input type="checkbox" name="services[]" id="checkbox24"-->
<!--                                                       value="チラシ・パンフレット作成">-->
<!--                            チラシ・パンフレット作成</label> <label><input type="checkbox" name="services[]" id="checkbox25"-->
<!--                                                               value="写真加工">-->
<!--                            写真加工</label> -->
<!--                    </div>-->
<!--                </fieldset>-->
            </div>

            <legend><label for="comment">お問い合わせ・ご要望内容</label></legend>
            <input type="hidden" name="referrer" value="(スマホ : <?php echo $_SERVER['PHP_SELF']; ?>)">
            <textarea name="comment" id="comment" cols="50" rows="5" placeholder="お問い合わせ内容・ご要望などをご記入ください"></textarea>
            <input type="submit" name="submit" value="　送信する　" style="padding:15px 40px;background-color:darkorange !important;color:#fff;border-style:none;border-bottom:4px solid darkred">
            <p>※ メールいただいた方には、確認のため電話を差し上げる場合がございます。</p>
        </div><!--.small-12 columns -->
    </div><!--.row -->
</form>