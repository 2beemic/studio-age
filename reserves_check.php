<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>証明写真・記念写真・商品撮影のスタジオアージュ　-お問い合わせ-</title>
<style type="text/css">
@charset "utf-8";

//form

#form1 {
	text-align:left;
}
#form_table {
	text-align: left;	
}

#asta {
	color:#999;
}

#check {
	font-weight: bold;
}
</style>
<link href="stage.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="header">
  <div align="left"><a href="index.html"><img src="images/header.jpg" alt="スタジオアージュ" width="427" height="69" border="0" /></a> 　　<a href="sitemap.html">サイトマップ</a>　<a href="access.html">アクセス</a>　<a href="support.html">お問合せ</a></div>
</div>
<div id="contents">
  <h1 align="center"><img src="images/sup_title.jpg" width="299" height="52" alt="お問い合わせ" /></h1>

    	<form action="./reserves.php" method="post" id="form1" name="form1">
      <table width="585" border="0" align="center" cellspacing="5" id="form_table">
    <tr>
      <td><div align="right"><span id="asta">*</span> お名前</div></td>
      <td id="check">
      	<?php print <<<EOF
{$in['name']}
<input type="hidden" value="{$in['name']}" name="name" />      	
EOF;
?>
         
      </td>
    </tr>
    
     <tr>
      <td><div align="right"><span id="asta">*</span> 性別</div></td>
      <td id="check">
      	<?php print <<<EOF
{$in['sex']}
<input type="hidden" value="{$in['sex']}" name="sex" />      	
EOF;
?>
         
      </td>
    </tr>
    
    <tr>
      <td><div align="right"><span id="asta">*</span> 電話番号</div></td>
      <td id="check">
      	<?php print <<<EOF
{$in['telephone']}
<input type="hidden" value="{$in['telephone']}" name="telephone" />
EOF;
?>
      </td>
    </tr>
    <tr>
      <td><div align="right"><span id="asta">*</span> メールアドレス</div></td>
      <td id="check">
      	<?php print <<<EOF
{$in['email']}
<input type="hidden" value="{$in['email']}" name="email" />
EOF;
?>
        </td>
    </tr>
    <tr>
      <td width="194"><div align="right">撮影希望日・時間</div></td>
      <td width="372" id="check">
		<?php

if($in['year']){
	print <<<EOF
{$in['year']}年{$in['month']}月{$in['date']}日 {$in['hour']}時{$in['minute']}分
<input type="hidden" value="{$in['year']}" name="year" />
<input type="hidden" value="{$in['month']}" name="month" />
<input type="hidden" value="{$in['date']}" name="date" />
<input type="hidden" value="{$in['hour']}" name="hour" />
<input type="hidden" value="{$in['minute']}" name="minute" />
EOF;
} else {
	print <<<EOF
{$in['ymd']} {$in['hour']}:{$in['minute']}
<input type="hidden" value="{$in['ymd']}" name="ymd" />
<input type="hidden" value="{$in['date']}" name="date" />
<input type="hidden" value="{$in['hour']}" name="hour" />
<input type="hidden" value="{$in['minute']}" name="minute" />
EOF;
}
?>
      </td>
    </tr>
    <tr>
      <td width="194"><div align="right">サービス内容</div></td>
      <td width="372" id="check">
        <?php
foreach($services_r as $service){
	print <<<EOF
{$service}<br />
<input type="hidden" value="{$service}" name="services[]" />
EOF;
}
        ?>
        &nbsp;
        </td>
    </tr>
    <tr>
      <td width="194"><div align="right">お問い合わせ内容<br />
      </div></td>
      <td width="372" id="check">
      	<?php
print <<<EOF
{$in['comment']}
<input type="hidden" value="{$in['comment']}" name="comment" />
EOF;
?>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <input type="button" onclick="history.back();" value="←戻る" />
      	<input type="submit" name="submit" value="　送信する→　" />
      	<input type="hidden" name="check" value="true" />
      </td>
    </tr>
      </table>
  </form>
  </p>
</div>
<div id="footer"><a href="sitemap.html">サイトマップ</a>-<a href="aboutus.html">会社概要</a>-<a href="policy.html">プライバシーポリシー</a>-<a href="access.html">アクセス</a>-<a href="support.html">お問合せ<br />
</a>Copyright (C) 2009 スタジオアージュ.</div>
<?php include("footer.html"); ?>

</body>
</html>
