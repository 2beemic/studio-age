  var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "phone_number", {validateOn:["blur"], format:"phone_custom"});
  var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none", {validateOn:["blur"]});
  var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3","email", {validateOn:["blur"]});

  $("#button__portrait").click(function(){
    $("#show_portrait").animate(
        {height: "toggle", opacity: "toggle"},
        "slow"
    );
    var win = $('html,body');
    var obj = $(event.target);
    var top = obj.offset().top - 70;
    win.animate({ 'scrollTop': top }, 600);
    event.preventDefault();
  });
  $("#button__shoumei").click(function(){
    $("#show_shoumei").animate(
        {height: "toggle", opacity: "toggle"},
        "slow"
    );
    var win = $('html,body');
    var obj = $(event.target);
    var top = obj.offset().top - 70;
    win.animate({ 'scrollTop': top }, 600);
    event.preventDefault();
  });

  $("#button__etc").click(function(){
    $("#show_etc").animate(
        {height: "toggle", opacity: "toggle"},
        "slow"
    );
    var win = $('html,body');
    var obj = $(event.target);
    var top = obj.offset().top - 70;
    win.animate({ 'scrollTop': top }, 600);
    event.preventDefault();
  });
  function UpdateState() {
      value = $("input[name='type']:checked").val();

      switch (value) {
        case "ご予約":
          $("#reserved_part").css("display","block");
          break;
        default:
          $("#reserved_part").css("display","none");
          break;
      }
    }

    $(document).ready(function () {
      UpdateState();
    });

    $(document).ready(function () {
      $("input[name='type']").click(function () {
        UpdateState();
      });
    });

    $('a[href*="#"]').click(function() {
      var target = $(this.hash);
      //if (target.length) {
      if (target) {
          var targetOffset = target.offset().top - 44;
          $('html,body').animate({ scrollTop: targetOffset},{ duration: 1000, easing: 'easeOutQuart' });
          return false;
        }
      });
    $(function(){
      $(window).scroll(function(){
        var now = $(window).scrollTop();
        var under = $('body').height() - (now + $(window).height());
        var form = $('#form:first');
        var offset = form.offset();
        var ask_top = offset.top - 200;
        if ( now > ask_top ){
          $('#ask_jump').fadeOut('slow');
        } else {
          $('#ask_jump').fadeIn('slow');
        }
      });
    });
    $(function(){
      $('.boy').swipebox();
      $('.girl').swipebox();
      $('.three').swipebox();
      $('.five').swipebox();
      $('.seven').swipebox();
      $('.denchi').swipebox();

    });
    $(document).on("pagecreate", function(event) {
    $(this).find(".boy img", ".girl img").lazyload({
            effect : "fadeIn",
            event: "scrollstop" // jQuery Mobile 固有実装
        });
    });
    $(document).bind("mobileinit", function(){
      $.extend( $.mobile , {
        ajaxLinksEnabled: false,
        ajaxFormsEnabled: false
      });
    });
  // window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
  // $(document).foundation();