<?php

$ua = getenv('HTTP_USER_AGENT');
if(preg_match("/iphone|android|mobile/i",$ua)){
	header("Location: https://studio-age.com/sp/maps.html");
	exit;
}

// google api key ABQIAAAAIbqlZmMBVSVVExGHM49GPRSF8bH2QIGJ3tkLuqRcN2QAhyy_AxRPopGnTWdHHpUkiTe-T-TO2z6D6Q
$api_key = "ABQIAAAAIbqlZmMBVSVVExGHM49GPRT_DZPxeIRImtKoe-9QQfj1fCxrfRTViYWZJcfU4T8FGo0YM7Q6vhbSkQ";

// 携帯かどうか確認する。
$user_agent = $_SERVER['HTTP_USER_AGENT'];
if(preg_match("/docomo|l-mode|astel|up\.browser|j-phone|Vodafone|SoftBank|KDDI|nitro|Nokia/i",$user_agent))
	$mobile++;
else
	$pc = true;

if(preg_match("/softbank|vodafone|j-phone/i",$user_agent))
	$softbank = true;

if(!$_GET['zoom']) $zoom = 18;
else $zoom = htmlspecialchars($_GET['zoom']);

if($pc)
	header("Location: http://maps.google.com/maps?f=q&source=s_q&hl=ja&geocode=&q=%E5%A4%A7%E9%98%AA%E5%B8%82%E5%8C%97%E5%8C%BA%E5%A4%A9%E7%A5%9E%E6%A9%8B2%E4%B8%81%E7%9B%AE3%E7%95%AA22%E5%8F%B7&sll=34.695897,135.511637&sspn=0.011997,0.020063&ie=UTF8&ll=34.696955,135.511594&spn=0.011997,0.020063&t=h&z=16");
else {
	$r = rand(1,10000);
	$url = "http://maps.google.com/staticmap?center=34.696954,135.511595&zoom={$zoom}&size=230x230&maptype=mobile&markers=34.696954,135.511594,redy&key={$api_key}&rn={$r}";
	if($softbank)
		$url = "http://maps.google.com/staticmap?center=34.696954,135.511595&zoom={$zoom}&size=230x230&maptype=mobile&markers=34.696954,135.511594,redy&marptype=mobile&format=png32&key={$api_key}";

	if($zoom >= 2) {
		$small_zoom = $zoom - 1;
		$zoom_html .= <<<EOF
<img src="/images/minus_icon.jpg" alt="-" /><a href="{$_SERVER['PHP_SELF']}?zoom={$small_zoom}">縮小</a>
EOF;
	}

	if($zoom <= 18) {
		$big_zoom = $zoom + 1;
		$zoom_html .= <<<EOF
 <a href="{$_SERVER['PHP_SELF']}?zoom={$big_zoom}">拡大</a><img src="/images/plus_icon.jpg" alt="-" />
EOF;
	}

	header("Content-Type: text/html; charset=utf-8");
	print <<<EOF
<html>
<head>
<meta name="Content-Type" content="utf-8" />
<title>スタジオアージュ地図</title>
</head>
<body>
<center>
<h1><font size="3">スタジオアージュ地図</font></h1>
<a href="{$url}"><img src="{$url}" alt="{$url}" /></a><br />
{$zoom_html}
</center>
<font size="2" color="#666666">
	〒530-0041<br />
	大阪市北区天神橋2丁目3番22号　西川ビル2F<br />
	スタジオ アージュ
</font>
</body>
</html>
EOF;
}
exit;
?>