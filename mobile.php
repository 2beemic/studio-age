<?php
$php_self = $_SERVER['PHP_SELF'];
$page = htmlspecialchars($_GET['page']);
if(!$page) {
	header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}?page=top");
	exit;
}
$menu_r = array(
	"top" => "証明写真を携帯ケータイで : 焼き増し4枚 → ナント30円から!!",
	"job" => "就職活動の証明写真について",
	"talk" => "友達に教える",
	"faq" => "Q&amp;A",
	"hint" => "使い方のヒント",
	"print" => "印刷の仕方",
	"app" => "使用するアプリケーション",
	"sheet" => "履歴書用の使えるシール印刷用紙(タスポは除く)",
	"howtouse" => "使い方",
	"merit" => "メリット",
);
$title = $menu_r[$page];
if($page == 'top'){
	$description = <<<EOF
<meta name="description" content="カメラ付き携帯で撮った写真をメールに添付してアップすると履歴書用もしくは タスポ(taspo)用の証明写真にして印刷できる無料サービスです。印刷は多くのパソコンに入っているソフトで行うので、インストール作業は不要!好きな数だけ印刷できてスピード写真よりも格安に出来ます。" />
EOF;
}
?>
<html lang="ja">
<base href="http://2php.jp/" />
<head>
<meta http-equiv="content-type" content="text/html;charset=shift_jis" />
<title><?= $menu_r[$page]; ?></title>
<meta name="keywords" content="携帯電話,携帯,写メ,証明写真,<?php echo $menu_r[$page]; ?>" />
<style type="text/css">
<!-- h2 { color: #ff9966;font-size:100%; } #menu li,#menu ul { list-style:none;padding:0; } -->
</style>
</head>
<body>
<h1 style="background:#ff8155;color:white;font-size:100%;text-align:right;"><?= $menu_r[$page]; ?></h1>
<div align="center"><img src="/shoumeishashin/images/mobile_title.gif" alt="おウチで証明写真" border="0" width="400" height="66" /></div>
<?php
if($page == 'top'){
?>
<div align="center"><img src="/shoumeishashin/images/sample.jpg" alt="サンプル" width="206" height="150" /></div>
<p>「おウチで<strong>証明写真</strong>」は<strong>カメラ付き携帯で自分を撮って、そのまま<a href="mailto:j@2php.jp">j@2php.jp</a>(履歴書用)もしくは<a href="mailto:t@2php.jp">t@2php.jp</a>(タスポ[taspo]用)に画像を添付してアップするだけで証明写真もしくはタスポ用</strong>のサイズになります。印刷はプリンタなので好きな分だけ印刷できますし、なによりもスピード写真で撮影するよりも安くできてオトクです。</p>
<?
} else {
	$file = "./dat/{$page}.dat";
	if(!is_file($file)) print "none";
	else {
		$html_r = file($file);
		$html = implode($html_r);
		print $html;
	}
}
?>
<hr />
<?php include("./mobile_menu.inc.php"); ?>


<?php
include("/home/homepage/public_html/footer.inc");
print $footer;
?>
</body>
</html>