<?php

$dir = "/home/homepage/public_html/photos/";
$pdf_dir = "/home/homepage/public_html/pic/";

$move_dir = "/home/homepage/old_photos/";

$i = 0;
$dp = @opendir($dir);
if($dp){
	while(false !== ($file = readdir($dp))){
		if($file == '.' || $file == '..' || $file == 'view.php') continue;
		$img_file = $dir . $file;
		$move_file = $move_dir . $file;
		
		$ymd = date("Ymd",filectime($img_file));
		$two_week = date("Ymd",mktime(0,0,0,date(m),date(d) - 14,date(Y)));
		
		if($ymd <= $two_week){
			system("mv {$img_file} {$move_file}");
//			print "{$img_file} : {$move_file} : {$ymd}<br />";
			// PDFファイルの削除
			$pdf_file = $file;
			if(!preg_match("/smp\.jpg/",$pdf_file)){
				$pdf_file = $pdf_dir . preg_replace("/\.jpg$/i",".pdf",$pdf_file);
				if(is_file($pdf_file)){
					unlink($pdf_file);
//					print "{$pdf_file} <br />";
				}
			}
			$i++;
//			if($i >= 10) break;
		}
	}
}

header("Location: http://2bee.jp/images/spacer.gif");
exit;
?>